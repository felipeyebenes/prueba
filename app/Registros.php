<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registros extends Model {

    protected $table = 'registros';
    protected $fillable = ['name'];
    protected $guarded = ['id'];

}
