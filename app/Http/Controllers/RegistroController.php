<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Registros;

class RegistroController extends BaseController {

    /**
     * Función principal que pinta todos los registros en el template home.blade
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    
    function index() {

        $registros = Registros::all();

        return view('home')
                        ->with('registros', $registros);
    }

    /**
     * Función que retorna la vista para agregar registros
     * 
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function agregar() {

        return view('registro/agregar');
    }

    /**
     * Función que retorna la vista editar, rellenada con los datos del registro, para poder editarlos
     * 
     * @param int $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function editar($id) {

        $registro = Registros::find($id);
        return view('registro/editar')
                        ->with('registro', $registro);
    }

    /**
     * Función que inserta registros en la BDD
     * 
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function insert(Request $request) {

        $name = $request->input('name');

        DB::table('registros')->insert(
                ['name' => $name]
        );

        return redirect()->action('RegistroController@index');
    }

    /**
     * Función que borrar registros de BDD
     * 
     * @param int $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     * 
     */
    function borrar($id) {

        $registros = Registros::find($id);
        $registros->delete();

        return redirect()->action('RegistroController@index');
    }

    /**
     * Función que actualiza un registro en la BDD
     * 
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function update(Request $request) {

        $id = $request->input('id');
        $name = $request->input('name');

        $registros = Registros::find($id);
        $registros->name = $name;
        $registros->save();

        return redirect()->action('RegistroController@index');
    }


}
