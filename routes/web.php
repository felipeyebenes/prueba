<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('home');
//});

Route::get('/', 'RegistroController@index');

Route::get('/agregar', 'RegistroController@agregar');

Route::get('/insert', ['as' => 'insert', 'uses' => 'RegistroController@insert']);

Route::get('/borrar/{id}', 'RegistroController@borrar');

Route::get('/editar/{id}', 'RegistroController@editar');

Route::get('/update', ['as' => 'update', 'uses' => 'RegistroController@update']);
