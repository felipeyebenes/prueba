@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listado de registros añadidos</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th> id</th>
                                <th> name</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($registros as $registro)
                            <tr>
                                <td> {{$registro->id}} </td>
                                <td> {{$registro->name}} </td>
                                <td>
                                    <a href="{{url('editar/'.$registro->id)}}" class="btn btn-warning">Editar</button></a>
                                </td>
                                <td>
                                    <a href="{{url('borrar/'.$registro->id)}}" class="btn btn-danger">Borrar</button></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>                    
                    <a href="{{ url('agregar') }}"><button type="button" class="btn btn-primary">Agregar registro</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
